// Header: ws2818 pwm based driver
// File Name: ws2818.c
// Author: Kargapolcev M. E.
// Date: 18.12.2018

#include "ws2812.h"
#include <string.h>

void ws2812_init(void);
const uint8_t bytes = 8;
const uint8_t pixels_num = bytes*3;

// Preambula min 50 uS (40*1.25uS)
#define PREAMBULA     50
// Pusle of high & low
#define WS2812_HIGH   21
#define WS2812_LOW    9

uint32_t framebuffer[PREAMBULA+pixels_num];   // OVERHAED!!! 1 bit-> 4 bytes!!!

void ws2812_init(void)
{
  memset(framebuffer, 0, PREAMBULA);
  int i = 0;
  // Green
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_HIGH;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  // Red
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  // Blue
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  framebuffer[PREAMBULA+i++] = WS2812_LOW;
  
  //HAL_TIM_PWM_Start(&htim4, 0); TIM4->CCR1 = 21;
  HAL_TIM_PWM_Start_DMA(&htim4, TIM_CHANNEL_1, (uint32_t*)framebuffer, sizeof(framebuffer) / sizeof(uint32_t)); //sizeof(framebuffer) / sizeof(uint32_t)
}
