// Header: ws2818 pwm based driver
// File Name: ws2818.h
// Author: Kargapolcev M. E.
// Date: 18.12.2018

#ifndef WS2812 
#define WS2812

#include "main.h"
#include "tim.h"

extern void ws2812_init(void);

#endif /* WS2812 */
